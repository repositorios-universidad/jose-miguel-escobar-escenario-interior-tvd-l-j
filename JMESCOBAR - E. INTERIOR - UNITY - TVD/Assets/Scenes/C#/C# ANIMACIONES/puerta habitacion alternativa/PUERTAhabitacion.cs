using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUERTAH : MonoBehaviour
{
    public Animator LaPuertaH;

    private void OnTriggerEnter(Collider other)
    {
        LaPuertaH.Play("habitacion1");
    }

    private void OnTriggerExit(Collider other)
    {
        LaPuertaH.Play("habitacion2");
    }
}
