using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUERTASpr2 : MonoBehaviour
{
    public Animator LaPuertapr2;

    private void OnTriggerEnter(Collider other)
    {
        LaPuertapr2.Play("pprincipal3");
    }

    private void OnTriggerExit(Collider other)
    {
        LaPuertapr2.Play("pprincipal4");
    }
}
