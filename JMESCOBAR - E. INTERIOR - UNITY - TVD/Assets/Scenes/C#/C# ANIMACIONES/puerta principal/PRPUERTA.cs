using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUERTASpr : MonoBehaviour
{
    public Animator LaPuertapr;

    private void OnTriggerEnter(Collider other)
    {
        LaPuertapr.Play("pprincipal1");
    }

    private void OnTriggerExit(Collider other)
    {
        LaPuertapr.Play("pprincipal2");
    }
}
