using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gabinet : MonoBehaviour
{
    public Animator gabinete;

    private void OnTriggerEnter(Collider other)
    {
        gabinete.Play("gabinet");
    }

    private void OnTriggerExit(Collider other)
    {
        gabinete.Play("gabinet2");
    }
}
