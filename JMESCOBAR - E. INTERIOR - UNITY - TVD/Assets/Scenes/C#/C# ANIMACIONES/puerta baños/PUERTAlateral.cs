using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUERTALATERAL : MonoBehaviour
{
    public Animator LaPuertaL;

    private void OnTriggerEnter(Collider other)
    {
        LaPuertaL.Play("puerta lateral 1");
    }

    private void OnTriggerExit(Collider other)
    {
        LaPuertaL.Play("puerta lateral 2");
    }
}
