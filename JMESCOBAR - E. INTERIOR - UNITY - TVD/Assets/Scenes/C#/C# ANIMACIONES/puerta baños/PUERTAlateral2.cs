using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUERTALATERAL2 : MonoBehaviour
{
    public Animator LaPuertaL2;

    private void OnTriggerEnter(Collider other)
    {
        LaPuertaL2.Play("puerta lateral 3");
    }

    private void OnTriggerExit(Collider other)
    {
        LaPuertaL2.Play("puerta lateral 4");
    }
}
