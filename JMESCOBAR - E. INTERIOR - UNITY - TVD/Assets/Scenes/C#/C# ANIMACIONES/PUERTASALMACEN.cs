using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUERTAS : MonoBehaviour
{
    public Animator LaPuerta;

    private void OnTriggerEnter(Collider other)
    {
        LaPuerta.Play("puerta almacen 1");
    }

    private void OnTriggerExit(Collider other)
    {
        LaPuerta.Play("puerta almacen 2");
    }
}
