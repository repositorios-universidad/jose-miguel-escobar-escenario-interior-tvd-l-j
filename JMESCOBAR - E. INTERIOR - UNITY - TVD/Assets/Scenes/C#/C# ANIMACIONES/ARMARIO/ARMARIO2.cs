using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARMARIO2 : MonoBehaviour
{
    public Animator armariop2;

    private void OnTriggerEnter(Collider other)
    {
        armariop2.Play("armario 3");
    }

    private void OnTriggerExit(Collider other)
    {
        armariop2.Play("armario4");
    }
}
