using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARMARIO : MonoBehaviour
{
    public Animator ARMARIOp1;

    private void OnTriggerEnter(Collider other)
    {
        ARMARIOp1.Play("armario1");
    }

    private void OnTriggerExit(Collider other)
    {
        ARMARIOp1.Play("armario2");
    }
}
